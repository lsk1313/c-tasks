﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    class FootballTeam
    {
        public string Name { get; }

        public ICollection<FootballPlayer> FootballPlayers { get; } = new List<FootballPlayer>();

        public int TeamSkill { get; private set; }

        public byte GoalsPerGame { get; set; } 

        public Coach Coach { get; }

        public FootballTeam(string name)
        {
            Name = name;
        }

        public FootballTeam(string name, Coach coach)
        {
            Name = name;

            Coach = coach;
        }

        public void Add(FootballPlayer footballPlayer)
        {
            FootballPlayers.Add(footballPlayer);

            TeamSkill += footballPlayer.Skill;

            if (Coach != null)
            {
                TeamSkill = (int)(Coach.Luck * TeamSkill);
            }
        }

        public static void AllPlayersInTeam(FootballTeam team)
        {
            ToConsole(team.FootballPlayers.OrderBy(p => p.Name));
        }

        public static void AllPlayersInTeam(IEnumerable<FootballPlayer> players)
        {
            ToConsole(players.OrderBy(p => p.Name));
        }

        public static void AllPlayersOldestThirtyAndSortByDescending(FootballTeam team)
        {
            ToConsole(team.FootballPlayers.Where(p => p.Age >= 30).OrderByDescending(p => p.Skill));
        }

        public static void AllPlayersOldestThirtyAndSortByDescending(IEnumerable<FootballPlayer> players)
        {
            ToConsole(players.Where(p => p.Age >= 30).OrderByDescending(p => p.Skill));
        }

        public static void AllPlayersOldestThirtyAndSortByRising(FootballTeam team)
        {
            ToConsole(team.FootballPlayers.Where(p => p.Age >= 30).OrderBy(p => p.Skill));
        }

        public static void AllPlayersOldestThirtyAndSortByRising(IEnumerable<FootballPlayer> players)
        {
            ToConsole(players.Where(p => p.Age >= 30).OrderBy(p => p.Skill));
        }

        private static void ToConsole(IEnumerable<FootballPlayer> list)
        {
            foreach (var player in list)
            {
                Console.WriteLine($"{player.Name}. Skill: {player.Skill}");
            }

            Console.WriteLine(string.Empty);
        }
    }
}
