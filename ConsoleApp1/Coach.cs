﻿using System;

namespace ConsoleApp1
{
    class Coach : Рerson
    {
        public override string Name { get; set; }

        public double Luck { get; } = new Random().NextDouble() + 0.5;

        public Coach(string name)
        {
            Name = name;
        }
    }

}
