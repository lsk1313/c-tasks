﻿using System;

namespace ConsoleApp1
{
    class Referee : Рerson
    {
        public override string Name { get; set; }

        public Referee(string name)
        {
            Name = name;
        }

        public int RefereePreferences { get; } = new Random().Next(0, 2);

        public void Goal(FootballGame game)
        {
            game.Goal += Game_Goal;
        }

        private void Game_Goal(object sender, EventArgs e)
        {
            if (!(sender is FootballTeam team)) return;

            team.GoalsPerGame++;
            Console.WriteLine($"Team {team.Name} scored a goal");
        }

        public void Foul(FootballGame game)
        {
            game.Foul += Game_Foul;
        }

        private void Game_Foul(object sender, EventArgs e)
        {
            if (!(sender is FootballTeam team)) return;

            if (team.GoalsPerGame > 0)
            {
                team.GoalsPerGame--;
            }

            Console.WriteLine($"Foul: {team.Name}");
        }
    }
}
