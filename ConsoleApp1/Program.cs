﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            var coachTeamOne = new Coach("Zinedine Zidane");
            var coachTeamTwo = new Coach("Ernesto Valverde");

            var teamOne = new FootballTeam("Real Madrid", coachTeamOne);
            var teamTwo = new FootballTeam("Barcelona", coachTeamTwo);

            var listWithTeamOne = new List<FootballPlayer>
            {
                new FootballPlayer("Alphonse Areola",26),
                new FootballPlayer("Dani Carvajal",27),
                new FootballPlayer("Éder Militão",21),
                new FootballPlayer("Sergio Ramos",33),
                new FootballPlayer("Raphaël Varane",26),
                new FootballPlayer("Nacho",29),
                new FootballPlayer("Eden Hazard",28),
                new FootballPlayer("Toni Kroos",29),
                new FootballPlayer("Karim Benzema",31),
                new FootballPlayer("Luka Modrić",33),
                new FootballPlayer("Gareth Bale",30),
            };

            foreach (var player in listWithTeamOne)
            {
                teamOne.Add(player);
            }

            var listWithTeamTwo = new List<FootballPlayer>
            {
                new FootballPlayer("Alphonse Areola",26),
                new FootballPlayer("Dani Carvajal",27),
                new FootballPlayer("Éder Militão",21),
                new FootballPlayer("Sergio Ramos",33),
                new FootballPlayer("Raphaël Varane",26),
                new FootballPlayer("Nacho",29),
                new FootballPlayer("Eden Hazard",28),
                new FootballPlayer("Toni Kroos",29),
                new FootballPlayer("Karim Benzema",31),
                new FootballPlayer("Luka Modrić",33),
                new FootballPlayer("Gareth Bale",30),
            };

            foreach (var player in listWithTeamTwo)
            {
                teamTwo.Add(player);
            }

            var game = new FootballGame(teamOne, teamTwo);
            var referee = new Referee("Pierluigi Collina");
            var gameWithReferee = new FootballGame(teamOne, teamTwo, referee);

            try
            {
                gameWithReferee.StartGame();
            }
            catch (ExceptionalSituation e)
            {
                ToConsole(e.Messege);
            }

            

            //FootballTeam.AllPlayersInTeam(teamOne);
            //FootballTeam.AllPlayersOldestThirtyAndSortByRising(teamOne);
            //FootballTeam.AllPlayersOldestThirtyAndSortByDescending(teamOne);

        }

        private static void ToConsole(string s)
        {
            Console.WriteLine(s);
        }
    }
}
