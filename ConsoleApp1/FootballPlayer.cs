﻿using System;

namespace ConsoleApp1
{
    class FootballPlayer : Рerson
    {
        public override string Name { get; set; }
        public byte Age { get; }

        public int Skill { get; } = new Random().Next(1, 100);

        public FootballPlayer(string name, byte age)
        {
            Name = name;
            Age = age;
        }
    }
}
