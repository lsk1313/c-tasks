﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class ExceptionalSituation : Exception
    {
        public string Messege { get; }

        public ExceptionalSituation()
        {
            Messege = Situations[new Random().Next(0, Situations.Count)]; ;
        }

        private List<string> Situations { get;  } = new List<string>
        {
            "Rain - match will be rescheduled",
            "Snow - match will be rescheduled",
            "Aliens - match will be rescheduled",
            "Huge meteorite - match will be rescheduled. Maybe",
            "Zombies - match will be rescheduled",
            "Windy - match will be rescheduled"
        };
    }
}
