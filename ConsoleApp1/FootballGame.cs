﻿using System;

namespace ConsoleApp1
{
    class FootballGame
    {
        public event EventHandler Foul;

        public event EventHandler Goal;

        private int _tickets;

        public FootballTeam FootballTeam1 { get; }

        public FootballTeam FootballTeam2 { get; }

        public Referee Referee { get; }

        public FootballGame(FootballTeam team1, FootballTeam team2)
        {
            FootballTeam1 = team1;

            FootballTeam2 = team2;

            _tickets = new Random().Next(300, 600);
        }

        public FootballGame(FootballTeam team1, FootballTeam team2, Referee referee)
        {
            FootballTeam1 = team1;

            FootballTeam2 = team2;

            Referee = referee;

            _tickets = new Random().Next(300, 600);
        }

        public void StartGame()
        {
            Referee.Goal(this);
            Referee.Foul(this);

            switch (Referee?.RefereePreferences)
            {
                case 0:

                    while ((_tickets -= FootballTeam1.TeamSkill) > 0)
                    {
                        Goal?.Invoke(FootballTeam1, EventArgs.Empty);
                    }

                    while ((_tickets -= FootballTeam2.TeamSkill) > 0)
                    {
                        Goal?.Invoke(FootballTeam2, EventArgs.Empty);
                    }

                    if (_tickets > 588)
                    {
                        throw new ExceptionalSituation();
                    }

                    ToConsole(FootballTeam1, FootballTeam2);
                    return;
                case 1:

                    while ((_tickets -= FootballTeam1.TeamSkill) > 0)
                    {
                        Goal?.Invoke(FootballTeam1, EventArgs.Empty);
                    }

                    while ((_tickets -= FootballTeam1.TeamSkill) > 0)
                    {
                        Goal?.Invoke(FootballTeam2, EventArgs.Empty);
                    }
                    Foul?.Invoke(FootballTeam2, EventArgs.Empty);

                    if (_tickets > 588)
                    {
                        throw new ExceptionalSituation();
                    }

                    ToConsole(FootballTeam1, FootballTeam2);
                    return;
                case 2:

                    while ((_tickets -= FootballTeam1.TeamSkill) > 0)
                    {
                        Goal?.Invoke(FootballTeam1, EventArgs.Empty);
                    }

                    while ((_tickets -= FootballTeam1.TeamSkill) > 0)
                    {
                        Goal?.Invoke(FootballTeam2, EventArgs.Empty);
                    }
                    Foul?.Invoke(FootballTeam1, EventArgs.Empty);

                    if (_tickets > 588)
                    {
                        throw new ExceptionalSituation();
                    }

                    ToConsole(FootballTeam1, FootballTeam2);
                    return;
            }

            if (FootballTeam1.TeamSkill > FootballTeam2.TeamSkill)
            {
                ToConsole(
                    (double)(FootballTeam1.TeamSkill - FootballTeam2.TeamSkill) / FootballTeam2.TeamSkill > 0.1
                        ? $"Command: {FootballTeam1.Name} win"
                        : "Tie");
            }
            else
            {
                ToConsole(
                    (double)(FootballTeam2.TeamSkill - FootballTeam1.TeamSkill) / FootballTeam2.TeamSkill > 0.1
                        ? $"Command: {FootballTeam2.Name} win"
                        : "Tie");
            }
        }

        private static void ToConsole(string str)
        {
            Console.WriteLine($"Match have been over: {str}");
        }

        private static void ToConsole(FootballTeam teamOne, FootballTeam teamTwo)
        {
            if (teamOne.GoalsPerGame > teamTwo.GoalsPerGame)
            {
                Console.WriteLine($"Match have been ended with the score: {teamOne.GoalsPerGame} - {teamTwo.GoalsPerGame}\nTeam: {teamOne.Name} with coach: {teamOne.Coach.Name} win");

            }
            else if (teamOne.GoalsPerGame < teamTwo.GoalsPerGame)
            {
                Console.WriteLine($"Match have been ended with the score: {teamOne.GoalsPerGame} - {teamTwo.GoalsPerGame}\nTeam: {teamTwo.Name} with coach: {teamTwo.Coach.Name} win");
            }
            else
            {
                Console.WriteLine($"Match have been ended with the score: {teamOne.GoalsPerGame} - {teamTwo.GoalsPerGame}\nTie");

            }
        }
    }
}
